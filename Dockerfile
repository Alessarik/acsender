FROM gradle:5.4.1-jdk8

ENV ANDROID_HOME="/tmp/android-sdk"
ENV SDK_URL="https://dl.google.com/android/repository/sdk-tools-linux-3859397.zip"
ENV SDK_URL_SHA="444e22ce8ca0f67353bda4b85175ed3731cae3ffa695ca18119cbacef1c1bea0"

RUN mkdir "$ANDROID_HOME" \
      && cd "$ANDROID_HOME" \
      && curl -o sdk.zip $SDK_URL \
      && echo "$SDK_URL_SHA sdk.zip" | sha256sum -c - \
      && unzip -q sdk.zip \
      && rm sdk.zip

ENV ANDROID_VERSION=29
ENV ANDROID_BUILD_TOOLS_VERSION=29.0.2

RUN yes | $ANDROID_HOME/tools/bin/sdkmanager --licenses
RUN $ANDROID_HOME/tools/bin/sdkmanager --update
RUN $ANDROID_HOME/tools/bin/sdkmanager \
      "build-tools;${ANDROID_BUILD_TOOLS_VERSION}" \
      "platforms;android-${ANDROID_VERSION}" \
      "platform-tools"

WORKDIR /home/gradle/acsender

COPY acsender.jks   ./
COPY build.gradle   ./
COPY src        ./src/

ARG KEYPASS

RUN gradle assemble

RUN find . | grep '\.apk' | xargs ls -l

CMD [ "cat", "./build/outputs/apk/release/acsender-release.apk" ]
