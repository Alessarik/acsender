echo OFF

@echo CLEAN BIN FOLDER
del %DEV_HOME%\bin\*.* /Q /S

mkdir %DEV_HOME%\bin
mkdir %DEV_HOME%\bin\obj

echo .
@echo PACKAGE RESOURCES INTO JAVA FILES
set CALL=%AAPT_PATH%
set CALL=%CALL% package
set CALL=%CALL% -f
set CALL=%CALL% -m
set CALL=%CALL% -S %DEV_HOME%/src/main/res
set CALL=%CALL% -J %DEV_HOME%/src/main/java
set CALL=%CALL% -M %DEV_HOME%/src/main/AndroidManifest.xml
set CALL=%CALL% -I %ANDROID_JAR%
echo %CALL%
call %CALL%
IF NOT "%ERRORLEVEL%" == "0" ( EXIT /B %ERRORLEVEL% )

echo .
@echo JAVA COMPILATION
set CALL=%JAVA_HOME%\bin\javac.exe
set CALL=%CALL% -d %DEV_HOME%/bin/obj
set CALL=%CALL% -cp %ANDROID_JAR%
set CALL=%CALL% -sourcepath %DEV_HOME%/src/main/java
set CALL=%CALL% %DEV_HOME%/src/main/java/%PACKAGE_PATH%/*.java
echo %CALL%
call %CALL%
IF NOT "%ERRORLEVEL%" == "0" ( EXIT /B %ERRORLEVEL% )

echo .
@echo ARCHIVATION INTO DEX (SHOULD CALL)
set CALL=%DX_PATH%
set CALL=%CALL% --dex
set CALL=%CALL% --output=%DEV_HOME%\bin\classes.dex
set CALL=%CALL% %DEV_HOME%\bin\obj
echo %CALL%
call %CALL%
IF NOT "%ERRORLEVEL%" == "0" ( EXIT /B %ERRORLEVEL% )

echo .
@echo CREATE UNSIGNED APPLICATION
set CALL=%AAPT_PATH%
set CALL=%CALL% package
set CALL=%CALL% -f
set CALL=%CALL% -M %DEV_HOME%/src/main/AndroidManifest.xml
set CALL=%CALL% -S %DEV_HOME%/src/main/res
set CALL=%CALL% -I %ANDROID_JAR%
set CALL=%CALL% -F %DEV_HOME%/bin/unsigned.apk
set CALL=%CALL% %DEV_HOME%/bin
echo %CALL%
call %CALL%
IF NOT "%ERRORLEVEL%" == "0" ( EXIT /B %ERRORLEVEL% )

echo .
@echo SIGN APPLICATION
set CALL=%JAVA_HOME%\bin\jarsigner
set CALL=%CALL% -sigalg SHA1withRSA
set CALL=%CALL% -digestalg SHA1
set CALL=%CALL% -keystore %DEV_HOME%/%KEYSTORE%
set CALL=%CALL% -storepass %KEYPASS%
set CALL=%CALL% -keypass %KEYPASS%
set CALL=%CALL% -signedjar %DEV_HOME%/bin/%SIGNED%
set CALL=%CALL% %DEV_HOME%/bin/%UNSIGNED%
set CALL=%CALL% %KEYCHAIN%
call %CALL%
IF NOT "%ERRORLEVEL%" == "0" ( EXIT /B %ERRORLEVEL% )

echo .
@echo ZIP ALIGN
set CALL=%ALIGN_PATH%
set CALL=%CALL% -v
set CALL=%CALL% -z 4
set CALL=%CALL% bin\%SIGNED%
set CALL=%CALL% bin\%FINALAPK%
echo %CALL%
call %CALL%
IF NOT "%ERRORLEVEL%" == "0" ( EXIT /B %ERRORLEVEL% )
