#!/bin/sh

docker build --build-arg KEYPASS=$KEYPASS -t acsenderbuild .

if [ "0" -ne "$?" ]; then
  echo ""
  echo "!!! Build was failed !!!"
  exit 1
fi

docker run --rm acsenderbuild  >  ./acsender-release.apk
