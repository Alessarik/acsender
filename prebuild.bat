rem System variables
set JAVA_HOME="c:\Program Files\Java\jdk1.8"
set ANDROID_HOME=C:\DEV\Android\SDK
set DEV_HOME=%CD%

rem Build versions
set BUILD_TOOLS_VERSION=29.0.2
set PLATFORM_VERSION=29

rem Calculated variables
set AAPT_PATH=%ANDROID_HOME%\build-tools\%BUILD_TOOLS_VERSION%\aapt.exe
set DX_PATH=%ANDROID_HOME%\build-tools\%BUILD_TOOLS_VERSION%\dx.bat
set ALIGN_PATH=%ANDROID_HOME%\build-tools\%BUILD_TOOLS_VERSION%\zipalign.exe
set ANDROID_JAR=%ANDROID_HOME%\platforms\android-%PLATFORM_VERSION%\android.jar
set ADB=%ANDROID_HOME%\platform-tools\adb.exe

rem Package variables
set PACKAGE_PATH=ru/alessarik/acsender
set PACKAGE=ru.alessarik.acsender
set MAIN_CLASS=SendActivity

rem Signing variables
set KEYSTORE=acsender.jks
set KEYCHAIN=acsenderKey
set KEYPASS=%KEYPASS%

rem Result file names
set UNSIGNED=unsigned.apk
set SIGNED=signed.apk
set FINALAPK=acsender.apk
