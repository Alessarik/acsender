package ru.alessarik.acsender;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Activity for send picture to GIBDD
 */
public class GibddActivity extends Activity {

  private static Integer MAX_PROGRESS_VALUE = 14;

  public static GibddAsyncTask mTask = null;

  public static SharedPreferences mSharedPreferences;

  public  static String mSession = null;
  public  static String mFToken = null;

  private static String mClaim = null;
  private static String mRegNomer = null;
  private static String mAddress = null;
  private static String mEmail = null;
  private static String mSurname = null;
  private static String mName = null;
  private static String mTown = null;
  private static String mRegion = null;

  public  static String  mImageAbsPath = null;
  public  static Integer mFileId = null;

  public  static boolean mCheckAppeal       = false;
  public  static boolean mSendConfirmation  = false;
  public  static boolean mConfirmEmail      = false;

  protected void showMessage(String str) {
    Utils.logMessage(str);
    TextView message = (TextView) findViewById(R.id.usermessage);
    if (message != null) {
      message.setText(str);
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_gibdd);

    mSession = null;
    mFToken = null;
    mClaim = null;
    mImageAbsPath = null;
    mRegNomer = null;
    mAddress = null;
    mEmail = null;
    mSurname = null;
    mName = null;
    mTown = null;
    mRegion = null;

    try {
      mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

      if (mTask == null) {
        mTask = new GibddAsyncTask(GibddActivity.this, GibddAsyncTask.STATE.GET_FTOKEN,
                                    null, null, null, null, null, null, null);
        mTask.execute();
      }

      ProgressBar bar = (ProgressBar) findViewById(R.id.progressbar);
      if (bar != null) {
        bar.setVisibility(View.VISIBLE);
        bar.setMax(MAX_PROGRESS_VALUE);
      }

      TextView claimView = (TextView) findViewById(R.id.claim);
      if (claimView != null) {
        mClaim = Utils.getStringFromRawFile(this, R.raw.claim);
        claimView.setText(mClaim);
      }

      Intent intent = getIntent();
      mRegNomer = intent.getStringExtra(Utils.REG_NOMER);
      mImageAbsPath = intent.getStringExtra(Utils.IMAGE_ABS_PATH);
      if (!TextUtils.isEmpty(mImageAbsPath)) {
        Utils.logMessage(mImageAbsPath);
        ImageView view = (ImageView) findViewById(R.id.image_captcha);
        if (view != null) {
          view.setImageURI(Uri.parse(mImageAbsPath));
        }
      }
      mAddress = intent.getStringExtra(Utils.ADDRESS);
      mEmail = intent.getStringExtra(Utils.EMAIL);
      mSurname = intent.getStringExtra(Utils.SURNAME);
      mName = intent.getStringExtra(Utils.NAME);
      mTown = intent.getStringExtra(Utils.TOWN);
      mRegion = intent.getStringExtra(Utils.REGION);
    }
    catch (Exception ex) {
      showMessage(ex.toString());
    }
  }

  private void createGibddAsyncTask(boolean aToSend) {
    if (mTask != null) {
      return;
    }

    TextView captchaView = (TextView) findViewById(R.id.captcha);
    String captchaStr = captchaView != null ? captchaView.getText().toString() : null;
    if (captchaView != null) {
      captchaView.setError(null);
    }

    TextView emailcodeView = (TextView) findViewById(R.id.emailcode);
    String emailcodeStr = emailcodeView != null ? emailcodeView.getText().toString() : null;
    if (emailcodeView != null) {
      emailcodeView.setError(null);
    }

    if (TextUtils.isEmpty(mFToken)) {
      mTask = new GibddAsyncTask(GibddActivity.this, GibddAsyncTask.STATE.GET_FTOKEN,
                                  null, null, null, null, null, null, null);
      mTask.execute();
      return;
    }

    if (TextUtils.isEmpty(captchaStr)) {
      mTask = new GibddAsyncTask(GibddActivity.this, GibddAsyncTask.STATE.GET_CAPTCHA,
                                  mSession, mFToken, null, null, null, null, null);
      mTask.execute();
      return;
    }

    if (null == mFileId || 0 == mFileId) {
      mTask = new GibddAsyncTask(GibddActivity.this, GibddAsyncTask.STATE.UPLOAD_FILE,
                                  mSession, mFToken, captchaStr, null, mAddress, mRegNomer, mClaim);
      mTask.execute();
      return;
    }

    if (!mCheckAppeal) {
      mTask = new GibddAsyncTask(GibddActivity.this, GibddAsyncTask.STATE.CHECK_APPEAL,
                                  mSession, mFToken, captchaStr, null, mAddress, mRegNomer, mClaim);
      mTask.execute();
      return;
    }

    if (!mSendConfirmation) {
      mTask = new GibddAsyncTask(GibddActivity.this, GibddAsyncTask.STATE.SEND_CONFIRMATION,
                                  mSession, mFToken, captchaStr, null, mAddress, mRegNomer, mClaim);
      mTask.execute();
      return;
    }

    if (!mConfirmEmail) {
      mTask = new GibddAsyncTask(GibddActivity.this, GibddAsyncTask.STATE.CONFIRM_EMAIL,
                                  mSession, mFToken, captchaStr, emailcodeStr, mAddress, mRegNomer, mClaim);
      mTask.execute();
      return;
    }

    if (aToSend) {
      mTask = new GibddAsyncTask(GibddActivity.this, GibddAsyncTask.STATE.SEND_DATA,
                                  mSession, mFToken, captchaStr, emailcodeStr, mAddress, mRegNomer, mClaim);
      mTask.execute();
    }
  }

  public void OnButtonCheck(View view) {
    createGibddAsyncTask(false);
  }

  public void OnButtonGibdd(View view) {
    createGibddAsyncTask(true);
  }
}
