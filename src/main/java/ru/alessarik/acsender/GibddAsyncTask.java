package ru.alessarik.acsender;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Pair;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * AsyncTask for send picture to GIBDD
 */
public class GibddAsyncTask extends AsyncTask<Void, Helper, Void> {

  public enum STATE {
    EMPTY,             // 0
    GET_FTOKEN,        // 1
    GET_CAPTCHA,       // 2
    UPLOAD_FILE,       // 3
    CHECK_APPEAL,      // 4
    SEND_CONFIRMATION, // 5
    CONFIRM_EMAIL,     // 6
    SEND_DATA,         // 7
  }

  private STATE mtState = STATE.EMPTY;

  private GibddActivity mActivity;

  private String mtSession;
  private String mtAddress;
  private String mtCaptcha;
  private String mtEmail;
  private String mtSurname;
  private String mtName;
  private String mtTown;
  private String mtRegion;
  private String mtRegNomer;

  private String mtFToken;
  private String mtCode;
  private String mtPostUrl;
  private String mtSubject;
  private String mtAnswer;
  private String mtClaim;

  private Helper mtHelper;

  GibddAsyncTask(GibddActivity aActivity, STATE aState, String aSession,
                  String aToken, String aCaptcha, String aCode,
                  String aAddress, String aRegNomer, String aClaim) {
    mActivity = aActivity;
    mtSession = aSession;
    mtState = aState;
    mtFToken = aToken;
    mtCaptcha = aCaptcha;
    mtCode = aCode;
    mtAddress = aAddress;
    mtRegNomer = aRegNomer;
    mtClaim = aClaim;
    mtPostUrl = mActivity.getString(R.string.url_post);
    mtSubject = mActivity.getString(R.string.url_subject);
    mtAnswer = mActivity.getString(R.string.url_answer);
    mtHelper = new Helper(this);
  }

  public void publishProgress() {
    publishProgress(mtHelper);
    SystemClock.sleep(Utils.ASYNC_DELAY_TIME);
  }

  protected void onProgressUpdate(Helper... progress) {
    mActivity.showMessage(progress[0].getStatus());
    ProgressBar bar = (ProgressBar) mActivity.findViewById(R.id.progressbar);
    if (bar != null) {
      bar.setProgress(progress[0].getProgress());
    }
  }

  @Override
  protected void onPostExecute(final Void success) {
    if (!mtHelper.isNoError()) {
      mActivity.showMessage(mtHelper.getError());
      if (mtHelper.isOpenView()) {
        Intent intent = new Intent(mActivity, PrefActivity.class);
        intent.putExtra(Utils.ERROR_VIEW_ID, mtHelper.getErrorViewId());
        intent.putExtra(Utils.ERROR_VIEW_CODE, mtHelper.getErrorViewCode());
        mActivity.startActivityForResult(intent, Utils.PREF_ACTIVITY);
      } else if (mtHelper.getErrorViewId() != 0 && mtHelper.getErrorViewCode() != 0) {
        TextView view = (TextView) mActivity.findViewById(mtHelper.getErrorViewId());
        if (view != null) {
          view.setError(mActivity.getString(mtHelper.getErrorViewCode()));
          view.requestFocus();
        }
      }
    }
    if (STATE.GET_FTOKEN == mtState) {
      if (null != mtHelper.getSession()) {
        mActivity.mSession = mtHelper.getSession();
      }
      if (null != mtHelper.getFToken()) {
        mActivity.mFToken = mtHelper.getFToken();
      }
      if (null != mtHelper.getSession() && null != mtHelper.getFToken()) {
        mActivity.mTask = new GibddAsyncTask(mActivity, STATE.GET_CAPTCHA, mActivity.mSession,
                                              mActivity.mFToken, null, null, null, null, null);
        mActivity.mTask.execute();
        return;
      }
    }
    if (STATE.GET_CAPTCHA == mtState) {
      if (!TextUtils.isEmpty(mtCode)) {
        ImageView mImageView = (ImageView) mActivity.findViewById(R.id.image_captcha);
        if (mImageView != null) {
          Drawable drawable = mtHelper.getDrawable();
          mImageView.setImageDrawable(drawable);
        }
      }
    }
    if (STATE.UPLOAD_FILE == mtState) {
      if (mtHelper.getFileId() > 0) {
        mActivity.mFileId = mtHelper.getFileId();
      }
    }
    if (STATE.CHECK_APPEAL == mtState) {
      if (mtHelper.isCheckAppeal()) {
        mActivity.mCheckAppeal = true;
      }
    }
    if (STATE.SEND_CONFIRMATION == mtState) {
      if (mtHelper.isSendConfirmation()) {
        mActivity.mSendConfirmation = true;
      }
    }
    if (STATE.CONFIRM_EMAIL == mtState) {
      if (mtHelper.isConfirmEmail()) {
        mActivity.mConfirmEmail = true;
      }
    }
    if (!TextUtils.isEmpty(mtHelper.getClaim())) {
      TextView claimView = (TextView) mActivity.findViewById(R.id.claim);
      if (null != claimView) {
        claimView.setText(mtHelper.getClaim());
      }
    }
    mActivity.mTask = null;
  }

  @Override
  protected Void doInBackground(Void... params) {
    try {
      if (STATE.GET_FTOKEN == mtState) {
        getFToken();
      }
      if (STATE.GET_CAPTCHA == mtState) {
        getCaptcha();
      }
      if (STATE.CHECK_APPEAL == mtState
            || STATE.SEND_CONFIRMATION == mtState
            || STATE.CONFIRM_EMAIL == mtState
            || STATE.SEND_DATA == mtState) {
        mtHelper.setClaim(mtClaim);
        mtEmail   = getConfigValue(Utils.APP_SAVED_EMAIL,   R.id.email);
        mtSurname = getConfigValue(Utils.APP_SAVED_SURNAME, R.id.surname);
        mtName    = getConfigValue(Utils.APP_SAVED_NAME,    R.id.name);
        mtTown    = getConfigValue(Utils.APP_SAVED_TOWN,    R.id.town);
        mtRegion  = getConfigValue(Utils.APP_SAVED_REGION,  R.id.spinner_region);
        mtRegion  = Utils.parseRegionCode(mtRegion);
        checkRegion();
        checkAddress();
        checkFToken();
        if (STATE.CONFIRM_EMAIL == mtState) {
          checkCode();
        }
        checkCaptcha();
        generateClaim();
        mtHelper.setClaim(mtClaim);
        mtClaim += mActivity.getString(R.string.value_postscriptum);
      }
      if (STATE.UPLOAD_FILE == mtState) {
        uploadFile(mActivity.mImageAbsPath);
      }
      if (STATE.CHECK_APPEAL == mtState) {
        checkAppeal();
      }
      if (STATE.SEND_CONFIRMATION == mtState) {
        sendConfirmationEmail();
      }
      if (STATE.CONFIRM_EMAIL == mtState) {
        confirmEmail();
      }
      if (STATE.SEND_DATA == mtState) {
        sendData();
      }
    } catch (Exception ex) {
      mtHelper.setError(ex.toString());
    }
    return null;
  }

  protected void getFToken() throws Exception {
    mtHelper.setStatus(mActivity.getString(R.string.status_get_ftoken));

    RequestHelper request = RequestHelper.PostUrlEncodedRequest();

    String path = mActivity.getString(R.string.url_server) + mtPostUrl;
    request.setAddress(path);

    request.addProperty("Referer", path);
    request.addParam("agree", "on");
    request.addParam("step", "2");

    request.doRequest();

    String answer = request.getAnswer();

    String token = Utils.parseFToken(answer);
    if (null != token) {
      mtHelper.setFToken(token);
      mtHelper.setStatus(String.format(mActivity.getString(R.string.status_set_ftoken), token));
    } else {
      mtHelper.setError(mActivity.getString(R.string.error_parse_ftoken));
    }

    String session = request.getSession();
    if (null != session) {
      mtHelper.setSession(session);
      mtHelper.setStatus(String.format(mActivity.getString(R.string.status_set_session), session));
    } else {
      mtHelper.setError(mActivity.getString(R.string.error_parse_session));
    }
  }

  protected void getCaptcha() throws Exception {
    mtHelper.setStatus(mActivity.getString(R.string.status_get_captcha));
    mtCode = getCodeForCaptcha();
    if (!TextUtils.isEmpty(mtCode)) {
      getCaptchaImage(mtCode);
    } else {
      mtHelper.setStatus(mActivity.getString(R.string.error_parse_captcha));
    }
  }

  protected String getCodeForCaptcha() {
    String code = String.format("%s", mtFToken);
    mtHelper.setStatus(String.format(mActivity.getString(R.string.status_set_captcha), code));
    return code;
  }

  protected void getCaptchaImage(String code) throws Exception {
    String server = mActivity.getString(R.string.url_server);
    String path = server + mActivity.getString(R.string.url_captcha_image) + code;
    String referer = server + mActivity.getString(R.string.url_post);

    URL url = new URL(path);
    HttpURLConnection conn = (HttpURLConnection)url.openConnection();
    conn.setRequestProperty("Referer", referer);
    conn.setRequestProperty("Cookie", "session=" + mtSession);
    Utils.logMessage(String.format("%d - %s",
                      conn.getResponseCode(), conn.getResponseMessage()));
    if (200 == conn.getResponseCode()) {
      InputStream strm = conn.getInputStream();
      Drawable drawable = Drawable.createFromStream(strm, null);
      strm.close();
      mtHelper.setDrawable(drawable);
    } else {
      mtCode = null;
    }

    conn.disconnect();
  }

  protected void uploadFile(String filePath) throws Exception {
    mtHelper.setStatus(mActivity.getString(R.string.status_upload_file));

    RequestHelper request = RequestHelper.PostFormDataRequest();

    String path = mActivity.getString(R.string.url_server) + mActivity.getString(R.string.url_upload_file);
    request.setAddress(path);
    request.setSession(mtSession);

    request.addProperty("X-Requested-With", "XMLHttpRequest");

    request.uploadFile(filePath);

    request.addParam("input_name", "file");

    request.doRequest();

    mtHelper.setStatus(Utils.decodeRussian(request.getAnswer(), mActivity));

    if (Utils.checkSuccess(request.getAnswer())) {
      Integer id = Utils.parseFileId(request.getAnswer());
      if (id > 0) {
        mtHelper.setFileId(id);
        mtHelper.setStatus(String.format(mActivity.getString(R.string.status_file_uploaded), id));
      }
    }
  }

  protected void checkAppeal() throws Exception {
    mtHelper.setStatus(mActivity.getString(R.string.status_check_appeal));

    RequestHelper request = RequestHelper.PostFormDataRequest();

    String path = mActivity.getString(R.string.url_server) + mActivity.getString(R.string.check_appeal);
    request.setAddress(path);
    request.setSession(mtSession);

    request.addProperty("X-Requested-With", "XMLHttpRequest");

    request.addParam("region_code", "76");
    request.addParam("subunit", "66");
    request.addParam("email", mtEmail);
    request.addParam("firstname", mtName);
    request.addParam("surname", mtSurname);
    request.addParam("captcha", mtCaptcha);
    request.addParam("message", mtClaim);

    request.doRequest();

    mtHelper.setStatus(Utils.decodeRussian(request.getAnswer(), mActivity));

    if (Utils.checkSuccess(request.getAnswer())) {
      mtHelper.setCheckAppeal(true);
    }
  }

  protected void sendConfirmationEmail() throws Exception {
    mtHelper.setStatus(mActivity.getString(R.string.status_send_confirmation));

    RequestHelper request = RequestHelper.PostUrlEncodedRequest();

    String path = mActivity.getString(R.string.url_server) + mActivity.getString(R.string.url_send_confirmation);
    request.setAddress(path);
    request.setSession(mtSession);

    request.addProperty("X-Requested-With", "XMLHttpRequest");

    request.addParam("email", mtEmail);
    request.addParam("firstname", mtName);

    request.doRequest();

    mtHelper.setStatus(Utils.decodeRussian(request.getAnswer(), mActivity));

    if (Utils.checkSuccess(request.getAnswer())) {
      mtHelper.setSendConfirmation(true);
    }
  }

  protected void confirmEmail() throws Exception {
    mtHelper.setStatus(mActivity.getString(R.string.status_confirm_email));

    RequestHelper request = RequestHelper.PostUrlEncodedRequest();

    String path = mActivity.getString(R.string.url_server) + mActivity.getString(R.string.url_confirm_email);
    request.setAddress(path);
    request.setSession(mtSession);

    request.addProperty("X-Requested-With", "XMLHttpRequest");

    request.addParam("email", mtEmail);
    request.addParam("key", mtCode);

    request.doRequest();

    mtHelper.setStatus(Utils.decodeRussian(request.getAnswer(), mActivity));

    if (Utils.checkSuccess(request.getAnswer())) {
      mtHelper.setConfirmEmail(true);
    }
  }

  protected String getConfigValue(String aKey, Integer viewId) {
    String value = mActivity.mSharedPreferences.getString(aKey, "");
    mtHelper.setStatus(aKey + ": " + value);
    if (TextUtils.isEmpty(value)) {
      mtHelper.setError(mActivity.getString(R.string.error_field_required), true, viewId, R.string.error_field_required);
    }
    return value;
  }

  protected void checkRegion() {
    mtHelper.setStatus(mActivity.getString(R.string.status_check_region));
    if (TextUtils.isEmpty(mtRegion)) {
      mtHelper.setError(mActivity.getString(R.string.error_field_required), true, R.id.spinner_region, R.string.error_field_required);
    }
  }

  protected void checkAddress() {
    mtHelper.setStatus(mActivity.getString(R.string.status_check_address));
    if (TextUtils.isEmpty(mtAddress)) {
      mtHelper.setError(mActivity.getString(R.string.error_field_required), false, R.id.address, R.string.error_field_required);
    }
  }

  protected void checkFToken() {
    mtHelper.setStatus(mActivity.getString(R.string.status_check_ftoken));
    if (TextUtils.isEmpty(mtFToken)) {
      mtHelper.setError(mActivity.getString(R.string.error_field_required));
    }
  }

  protected void checkCode() {
    mtHelper.setStatus(mActivity.getString(R.string.status_check_code));
    if (TextUtils.isEmpty(mtCode)) {
      mtHelper.setError(mActivity.getString(R.string.error_field_required), false, R.id.emailcode, R.string.error_field_required);
    }
  }

  protected void checkCaptcha() {
    mtHelper.setStatus(mActivity.getString(R.string.status_check_captcha));
    if (TextUtils.isEmpty(mtCaptcha)) {
      mtHelper.setError(mActivity.getString(R.string.error_field_required), false, R.id.captcha, R.string.error_field_required);
    }
  }

  protected void generateClaim() {
    mtClaim = Utils.replacePattern(mtClaim, "[DATE]", Utils.extractDateFromPath(mActivity.mImageAbsPath));
    mtClaim = Utils.replacePattern(mtClaim, "[TIME]", Utils.extractTimeFromPath(mActivity.mImageAbsPath));
    mtClaim = Utils.replacePattern(mtClaim, "[REGNOMER]", mtRegNomer);
    mtClaim = Utils.replacePattern(mtClaim, "[TOWN]", mtTown);
    mtClaim = Utils.replacePattern(mtClaim, "[ADDRESS]", mtAddress);
  }

  protected void sendData() throws Exception {
    if (!mtHelper.isNoError()) {
      return;
    }

    mtHelper.setStatus(mActivity.getString(R.string.status_prepare_data));

    RequestHelper request = RequestHelper.PostFormDataRequest();

    String path = mActivity.getString(R.string.url_server) + mActivity.getString(R.string.url_post);
    request.setAddress(path);
    request.setSession(mtSession);

    request.addParam("region_code", "76");
    request.addParam("subunit", "66");
    request.addParam("firstname", mtName);
    request.addParam("surname", mtSurname);
    request.addParam("email", mtEmail);
    request.addParam("file", mActivity.mFileId.toString());
    request.addParam("captcha", mtCaptcha);
    request.addParam("message", mtClaim);
    request.addParam("step", "3");

    mtHelper.setStatus(mActivity.getString(R.string.status_connect));

    request.doRequest();

    Pair<Boolean, String> result = Utils.DecodeClaimResult(request.getAnswer());
    if (null != result) {
      String answer = Utils.decodeRussian(result.second, mActivity);
      mtHelper.setStatus(answer);
      if (result.first) {
        String decoded = Utils.DecodeHtmlContent(answer);
        mtHelper.setStatus(decoded);
        Utils.addSentCountStats(mActivity);
        Utils.sendTelemetry(mActivity);
        Utils.addAddressToCache(mActivity, mtAddress);
      }
    }
  }
}
