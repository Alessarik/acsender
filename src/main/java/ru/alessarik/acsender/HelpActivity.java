package ru.alessarik.acsender;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Activity for show instruction
 */
public class HelpActivity extends Activity {

  public static HelpAsyncTask mHelpTask = null;

  protected String mTextInstruction;

  protected void showMessage(String str) {
    Utils.logMessage(str);
    TextView message = (TextView) findViewById(R.id.usermessage);
    if (null != message) {
      message.setText(str);
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_help);

    try {
      showMessage(getString(R.string.status_instruction));
      mTextInstruction = getString(R.string.message_instruction);

      ProgressBar bar = (ProgressBar) findViewById(R.id.progressbar);
      if (null != bar) {
        bar.setVisibility(View.VISIBLE);
        bar.setMax(mTextInstruction.length());
      }

      View button = findViewById(R.id.button_gallery);
      if (null != button) {
        button.setVisibility(View.GONE);
      }

      if (null == mHelpTask) {
        mHelpTask = new HelpAsyncTask(this);
        mHelpTask.execute();
      } else {
        mHelpTask.setActivity(this);
      }

    } catch (Exception ex) {
      showMessage(ex.toString());
    }
  }

  public void onButtonGallery(View view) {
    Intent intent = new Intent();
    intent.setType("image/*");
    intent.setAction(Intent.ACTION_VIEW);
    startActivity(intent);
  }
}
