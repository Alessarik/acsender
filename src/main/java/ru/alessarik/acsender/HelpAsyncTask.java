package ru.alessarik.acsender;

import android.os.AsyncTask;
import android.os.SystemClock;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * AsyncTask for show instruction
 */
public class HelpAsyncTask extends AsyncTask<Void, Helper, Void> {

  private HelpActivity mActivity;
  private Integer   mtLength;
  private Helper    mtHelper;

  HelpAsyncTask(HelpActivity aActivity) {
    mActivity = aActivity;
    mtLength = mActivity.mTextInstruction.length();
    mtHelper = new Helper(this);
  }

  void setActivity(HelpActivity aActivity) {
    mActivity = aActivity;
  }

  @Override
  protected Void doInBackground(Void... params) {
    for (int i = 0; i < mtLength; i++) {
      mtHelper.setStatus(null);
    }
    return null;
  }

  public void publishProgress() {
    publishProgress(mtHelper);
    SystemClock.sleep(Utils.ASYNC_INSTRUCTION_DELAY);
  }

  protected void onProgressUpdate(Helper... progress) {
    ProgressBar bar = (ProgressBar) mActivity.findViewById(R.id.progressbar);
    if (null != bar) {
      bar.setProgress(progress[0].getProgress());
    }
    TextView textView = (TextView) mActivity.findViewById(R.id.instruction);
    if (null != textView) {
      Integer length = progress[0].getProgress();
      if (length >= mActivity.mTextInstruction.length()) {
        length = mActivity.mTextInstruction.length();
      }
      textView.setText(mActivity.mTextInstruction.substring(0, length));
    }
  }

  @Override
  protected void onPostExecute(final Void success) {
    ProgressBar bar = (ProgressBar) mActivity.findViewById(R.id.progressbar);
    if (null != bar) {
      bar.setVisibility(View.GONE);
    }
    View button = mActivity.findViewById(R.id.button_gallery);
    if (null != button) {
      button.setVisibility(View.VISIBLE);
    }
    mActivity.mHelpTask = null;
  }

  @Override
  protected void onCancelled() {
    mActivity.showMessage(mActivity.getString(R.string.error_send_cancelled));
    ProgressBar bar = (ProgressBar) mActivity.findViewById(R.id.progressbar);
    if (null != bar) {
      bar.setVisibility(View.GONE);
    }
    mActivity.mHelpTask = null;
  }
}
