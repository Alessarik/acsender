package ru.alessarik.acsender;

import android.graphics.drawable.Drawable;
import android.text.TextUtils;

public class Helper {
  private GibddAsyncTask  mGibddTask  = null;
  private HelpAsyncTask   mHelpTask   = null;
  private SendAsyncTask   mSendTask   = null;

  private boolean mNetwork          = false;
  private boolean mOpenView         = false;
  private boolean mCheckAppeal      = false;
  private boolean mSendConfirmation = false;
  private boolean mConfirmEmail     = false;
  private Integer mErrorViewCode  = 0;
  private Integer mErrorViewId    = 0;
  private Integer mProgress       = 0;
  private Integer mCounter        = 0;
  private Integer mFileId         = 0;
  private String  mClaim;
  private String  mError;
  private String  mLoginResponse;
  private String  mRegNomer;
  private String  mSession;
  private String  mSessionResponse;
  private String  mFToken;
  private String  mStatus;
  private Drawable mDrawable;

  public Helper(GibddAsyncTask gibddTask) {
    mGibddTask = gibddTask;
  }
  public Helper(HelpAsyncTask helpTask) {
    mHelpTask = helpTask;
  }
  public Helper(SendAsyncTask sendTask) {
    mSendTask = sendTask;
  }
  public Integer getProgress() {
    return mProgress;
  }
  public String getStatus() {
    return mStatus;
  }
  public void setStatus(String str) {
    mStatus = str;
    mProgress++;
    if (mGibddTask != null) {
      mGibddTask.publishProgress();
    }
    if (mHelpTask != null) {
      mHelpTask.publishProgress();
    }
    if (mSendTask != null) {
      mSendTask.publishProgress();
    }
  }
  public String getError() {
    return mError;
  }
  public void setError(String str) {
    if(TextUtils.isEmpty(mError)) {
      mError = str;
    }
  }
  public void setError(String str, boolean openView, Integer errorViewId, Integer errorMessageId) {
    if(isNoError()) {
      mError = str;
      mOpenView = openView;
      mErrorViewId = errorViewId;
      mErrorViewCode = errorMessageId;
    }
  }
  public boolean isOpenView() {
    return mOpenView;
  }
  public Integer getErrorViewId() {
    return mErrorViewId;
  }
  public Integer getErrorViewCode() {
    return mErrorViewCode;
  }
  public boolean getNetwork() {
    return mNetwork;
  }
  public void setNetwork(boolean flag) {
    mNetwork = flag;
  }
  public boolean isCheckAppeal() {
    return mCheckAppeal;
  }
  public void setCheckAppeal(boolean flag) {
    mCheckAppeal = flag;
  }
  public boolean isSendConfirmation() {
    return mSendConfirmation;
  }
  public void setSendConfirmation(boolean flag) {
    mSendConfirmation = flag;
  }
  public boolean isConfirmEmail() {
    return mConfirmEmail;
  }
  public void setConfirmEmail(boolean flag) {
    mConfirmEmail = flag;
  }
  public String getSessionResponse() {
    return mSessionResponse;
  }
  public void setSessionResponse(String str) {
    mSessionResponse = str;
  }
  public String getFToken() {
    return mFToken;
  }
  public void setFToken(String str) {
    mFToken = str;
  }
  public String getLoginResponse() {
    return mLoginResponse;
  }
  public void setLoginResponse(String str) {
    mLoginResponse = str;
  }
  public boolean isNoError() {
    return TextUtils.isEmpty(mError);
  }
  public String getRegNomer() {
    return mRegNomer;
  }
  public void setRegNomer(String str) {
    mRegNomer = str;
  }
  public String getSession() {
    return mSession;
  }
  public void setSession(String str) {
    mSession = str;
  }
  public void setDrawable(Drawable drawable) {
    mDrawable = drawable;
  }
  public Drawable getDrawable() {
    return mDrawable;
  }
  public void setCounter(Integer count) {
    mCounter = count;
  }
  public Integer getCounter() {
    return mCounter;
  }
  public void setFileId(Integer id) {
    mFileId = id;
  }
  public Integer getFileId() {
    return mFileId;
  }
  public void setClaim(String aClaim) {
    mClaim = aClaim;
  }
  public String getClaim() {
    return mClaim;
  }
}
