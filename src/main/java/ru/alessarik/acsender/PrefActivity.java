package ru.alessarik.acsender;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * Activity for show preferences
 */
public class PrefActivity extends Activity {

  private SharedPreferences mSharedPreferences;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    try {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_pref);

      Spinner regionView = (Spinner) findViewById(R.id.spinner_region);
      if (regionView != null) {
        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(this, R.array.region, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        regionView.setAdapter(arrayAdapter);
      }

      mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
      loadPreferenceValue(mSharedPreferences, Utils.APP_SAVED_EMAIL, R.id.email);
      loadPreferenceValue(mSharedPreferences, Utils.APP_SAVED_SURNAME, R.id.surname);
      loadPreferenceValue(mSharedPreferences, Utils.APP_SAVED_NAME, R.id.name);
      loadPreferenceValue(mSharedPreferences, Utils.APP_SAVED_TOWN, R.id.town);

      Intent intent = getIntent();
      int id = intent.getIntExtra(Utils.ERROR_VIEW_ID, 0);
      if(0 != id) {
        int code = intent.getIntExtra(Utils.ERROR_VIEW_CODE, 0);
        if(0 != code) {
          TextView view = (TextView) findViewById(id);
          if(null != view) {
            String error = getString(code);
            if(!TextUtils.isEmpty(error)) {
              view.setError(error);
            }
          }
        }
      }
    } catch (Exception ex) {
      Utils.logMessage(ex.toString());
    }
  }

  public void onButtonSave(View view) {
    Editor editor = mSharedPreferences.edit();
    savePreferenceValue(editor, Utils.APP_SAVED_EMAIL, R.id.email, null);
    savePreferenceValue(editor, Utils.APP_SAVED_SURNAME, R.id.surname, null);
    savePreferenceValue(editor, Utils.APP_SAVED_NAME, R.id.name, null);
    savePreferenceValue(editor, Utils.APP_SAVED_TOWN, R.id.town, null);
    savePreferenceValue(editor, Utils.APP_SAVED_REGION, null, R.id.spinner_region);
    editor.apply();

    finish(); // To close the form
  }

  private void loadPreferenceValue(SharedPreferences aPref, String aKey, Integer aViewId) {
    TextView view = (TextView) findViewById(aViewId);
    if(view != null) {
      String value = aPref.getString(aKey, "");
      view.setText(value);
    }
  }

  private void savePreferenceValue(Editor aEditor, String aKey, Integer aTextViewId, Integer aSpinnerViewId) {
    String value = null;
    if (aTextViewId != null) {
      TextView view = (TextView) findViewById(aTextViewId);
      if (view != null) {
        value = view.getText().toString();
      }
    } else if (aSpinnerViewId != null) {
      Spinner spinner = (Spinner) findViewById(aSpinnerViewId);
      if (spinner != null) {
        value = spinner.getSelectedItem().toString();
      }
    }
    if (value != null) {
      aEditor.putString(aKey, value);
    }
  }
}
