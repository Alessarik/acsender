package ru.alessarik.acsender;

import android.text.TextUtils;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RequestHelper {
  private String      mMethod;
  private ContentType mContentType;
  private String      mAddress;
  private String      mUploadFilePath;
  private HashMap<String, String> mParams;
  private HashMap<String, String> mProps;

  private ByteArrayOutputStream mRequestBodyByteArray;
  private DataOutputStream      mRequestBodyOutput;

  private Integer mResponseCode;
  private String  mAnswer;
  private Map<String, List<String>> mHeaders;

  private enum ContentType {
    FormData,
    UrlEncoded,
  }

  public static RequestHelper PostFormDataRequest() {
    RequestHelper requestHelper = new RequestHelper("POST");
    requestHelper.setContentType(ContentType.FormData);
    return requestHelper;
  }

  public static RequestHelper PostUrlEncodedRequest() {
    RequestHelper requestHelper = new RequestHelper("POST");
    requestHelper.setContentType(ContentType.UrlEncoded);
    return requestHelper;
  }

  public RequestHelper(String aMethod) {
    mMethod = aMethod;
    Utils.logMessage(aMethod);
  }

  public void setAddress(String aAddress) {
    mAddress = aAddress;
    Utils.logMessage(aAddress);
  }

  private void setContentType(ContentType aContentType) {
    mContentType = aContentType;
    Utils.logMessage(aContentType.toString());
    if (ContentType.FormData == aContentType) {
      this.addProperty("Content-Type", "multipart/form-data; boundary=" + Utils.BOUNDRY);
    }
    if (ContentType.UrlEncoded == aContentType) {
      this.addProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
    }
  }

  public void setSession(String aSession) {
    this.addProperty("Cookie", "session=" + aSession);
  }

  public void uploadFile(String path) {
    mUploadFilePath = path;
  }

  public void addParam(String aName, String aValue) {
    if (null == mParams) {
      mParams = new HashMap<String, String>();
    }
    mParams.put(aName, aValue);
  }

  public void addProperty(String aName, String aValue) {
    if (null == mProps) {
      mProps = new HashMap<String, String>();
    }
    mProps.put(aName, aValue);
  }

  protected void encodeFormData() throws Exception {
    byte[] imageData = null;
    StringBuffer requestBody = new StringBuffer();
    StringBuffer requestBodyEnd = new StringBuffer();

    if (null != mParams) {
      Utils.appendFormData(requestBody, "fix_issue_with", "empty_first_parameter");

      Object[] keys = mParams.keySet().toArray();
      for(int i = 0; i < keys.length; i++) {
        String key = keys[i].toString();
        String value = mParams.get(key);
        Utils.appendFormData(requestBody, key, value);
        Utils.logMessage("Parameter " + key + ": " + value);
      }

      if (!TextUtils.isEmpty(mUploadFilePath)) {
        imageData = Utils.appendFormFile(requestBody, "file", "Content-Type: image/jpeg", mUploadFilePath);
        Utils.logMessage("Parameter file: " + mUploadFilePath);
      }

      Utils.appendFormEnd(requestBodyEnd);
    }

    // Length in UTF-8 is more than length of string because it contains russian characters
    mRequestBodyByteArray = new ByteArrayOutputStream();
    mRequestBodyOutput = new DataOutputStream(mRequestBodyByteArray);
    mRequestBodyOutput.writeUTF(requestBody.toString());

    if (null != imageData) {
      mRequestBodyOutput.write(imageData);
    }

    mRequestBodyOutput.writeUTF(requestBodyEnd.toString());
  }

  public void encodeUrlData() throws Exception {
    StringBuffer requestBody = new StringBuffer();

    if (null != mParams) {
      requestBody.append("fix_issue_with=empty_first_parameter&");

      Object[] keys = mParams.keySet().toArray();
      for(int i = 0; i < keys.length; i++) {
        if (0 != i) {
          requestBody.append('&');
        }
        String key = keys[i].toString();
        String value = mParams.get(key);
        requestBody.append(key);
        requestBody.append('=');
        requestBody.append(URLEncoder.encode(value));
        Utils.logMessage("Parameter " + key + ": " + value);
      }
    }

    // Length in UTF-8 is more than length of string because it contains russian characters
    mRequestBodyByteArray = new ByteArrayOutputStream();
    mRequestBodyOutput = new DataOutputStream(mRequestBodyByteArray);
    mRequestBodyOutput.writeUTF(requestBody.toString());
  }

  public void doRequest() throws Exception {
    if (ContentType.FormData == mContentType) {
      encodeFormData();
    }
    if (ContentType.UrlEncoded == mContentType) {
      encodeUrlData();
    }

    int length = mRequestBodyOutput.size();
    this.addProperty("Content-Length", String.valueOf(length));

    URL url = new URL(mAddress);
    HttpURLConnection connection = (HttpURLConnection)url.openConnection();

    connection.setUseCaches(false);
    connection.setDoInput(true);
    connection.setDoOutput(true);
    connection.setInstanceFollowRedirects(false);
    connection.setRequestMethod(mMethod);

    if (null != mProps) {
      Object[] keys = mProps.keySet().toArray();
      for(int i = 0; i < keys.length; i++) {
        String key = keys[i].toString();
        String value = mProps.get(key);
        connection.setRequestProperty(key, value);
        Utils.logMessage("Property " + key + ": " + value);
      }
    }

    // Connect
    connection.connect();
    // Get stream for send request
    DataOutputStream dataOS = new DataOutputStream(connection.getOutputStream());
    // Send request body
    mRequestBodyByteArray.writeTo(dataOS);
    // Close sending stream
    dataOS.flush();
    dataOS.close();

    mResponseCode = connection.getResponseCode();
    Utils.logMessage(String.valueOf(mResponseCode));
    mHeaders = connection.getHeaderFields();
    mAnswer = Utils.getInputStreamAsString(connection);
    connection.disconnect();
    Utils.logMessage(String.valueOf(mAnswer));
    System.gc();
  }

  public Integer getResponseCode() {
    return mResponseCode;
  }

  public String getAnswer() {
    return mAnswer;
  }

  public String getSession() {
    if (null != mHeaders) {
      List<String> cookies = mHeaders.get("Set-Cookie");
      if (null != cookies) {
        for (String cookie : cookies) {
          String session = Utils.parseSession(cookie);
          if (!TextUtils.isEmpty(session)) {
            return session;
          }
        }
      }
    }
    return null;
  }
}
