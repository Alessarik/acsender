package ru.alessarik.acsender;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.android.gms.security.ProviderInstaller;
import java.util.ArrayList;
import java.util.List;
import javax.net.ssl.SSLContext;

/**
 * Activity for prepare sending pictures
 */
public class SendActivity extends Activity {

  private static Integer MAX_PROGRESS_VALUE = 9;

  public static SendAsyncTask mSendTask = null;

  public static SharedPreferences mSharedPreferences;

  private static List<String> mImageAbsPath;

  public SendActivity() {
    mImageAbsPath = new ArrayList<String>();
  }

  protected void showMessage(String str) {
    Utils.logMessage(str);
    TextView message = (TextView) findViewById(R.id.usermessage);
    if (message != null) {
      message.setText(str);
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_send);

    try {
      Utils.initialize(SendActivity.this);

      ProgressBar bar = (ProgressBar) findViewById(R.id.progressbar);
      if(bar != null) {
        bar.setVisibility(View.VISIBLE);
        bar.setMax(MAX_PROGRESS_VALUE);
      }

      TextView addressView = (TextView) findViewById(R.id.address);
      if (null != addressView) {
        String address = Utils.getMaxUsedAddress(this);
        addressView.setText(address);
      }

      mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

      mImageAbsPath.clear();
      Intent intent = getIntent();
      List<Uri> allPathes = new ArrayList<Uri>();

      Uri path = intent.getParcelableExtra(Intent.EXTRA_STREAM);
      if (path != null) {
        allPathes.add(path);
      }

      List<Uri> pathes = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
      if (pathes != null) {
        allPathes.addAll(pathes);
      }

      ImageView imageView = (ImageView) findViewById(R.id.image_car0);
      if (imageView != null) {
        boolean empty_image = true;
        for (int i = 0; i < allPathes.size(); i++) {
          String absPath = Utils.getAbsolutePath(imageView.getContext(), allPathes.get(i));
          if (absPath != null) {
            mImageAbsPath.add(absPath);
            if (empty_image) {
              imageView.setImageURI(allPathes.get(i));
              empty_image = false;
            }
          }
        }
      }

      int ids[] = {
                    R.id.image_car0, R.id.image_car1,
                    R.id.image_car2, R.id.image_car3,
                    R.id.image_car4, R.id.image_car5,
                    R.id.image_car6, R.id.image_car7,
                    R.id.image_car8, R.id.image_car9
                  };
      for (int i = 0; i < 10; i++) {
        ImageView iv = (ImageView) findViewById(ids[i]);
        if (iv != null) {
          if (i < mImageAbsPath.size()) {
            showMessage(Uri.parse(mImageAbsPath.get(i)).toString());
            iv.setImageURI(Uri.parse(mImageAbsPath.get(i)));
          }
        }
      }

      DoFixForWebRequestsWithSsl();

      if (mImageAbsPath.size() > 0) {
        showMessage(String.format(getString(R.string.status_loaded), mImageAbsPath.size()));
      } else {
        showMessage(getString(R.string.error_empty_image_path));
      }
    } catch (Exception ex) {
      showMessage(ex.toString());
    }
  }

  public void OnButtonCheck(View view) {
    CreateSendAsyncTask();
  }

  public void OnButtonPref(View view) {
    Intent intent = new Intent(SendActivity.this, PrefActivity.class);
    startActivityForResult(intent, Utils.PREF_ACTIVITY);
  }

  public void SendToGibdd(String regNomer, String address,
      String email, String surname, String name, String town, String region) {
    Intent intent = new Intent(SendActivity.this, GibddActivity.class);
    if (mImageAbsPath.size() > 0) {
      intent.putExtra(Utils.IMAGE_ABS_PATH, mImageAbsPath.get(0));
    }
    intent.putExtra(Utils.REG_NOMER, regNomer);
    intent.putExtra(Utils.ADDRESS, address);
    intent.putExtra(Utils.EMAIL, email);
    intent.putExtra(Utils.SURNAME, surname);
    intent.putExtra(Utils.NAME, name);
    intent.putExtra(Utils.TOWN, town);
    intent.putExtra(Utils.REGION, region);
    startActivityForResult(intent, Utils.GIBDD_ACTIVITY);
  }

  private boolean CreateSendAsyncTask() {
    if(mSendTask == null) {
      TextView nomerView = (TextView) findViewById(R.id.regnomer);
      String regNomerStr = nomerView != null ? nomerView.getText().toString() : null;

      TextView addressView = (TextView) findViewById(R.id.address);
      String addressStr = addressView != null ? addressView.getText().toString() : null;

      if (mImageAbsPath.size() > 0) {
        mSendTask = new SendAsyncTask(this, regNomerStr, addressStr, mImageAbsPath.get(0));
        mSendTask.execute();
      }
      return true;
    }
    return false;
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    // if(requestCode == Utils.AUTH_ACTIVITY) {
    //   if(data != null) {
    //     mSession = data.getStringExtra(AuthActivity.SESSION_NAME);
    //   }
    // }
  }

  protected void DoFixForWebRequestsWithSsl() throws Exception {
    showMessage("DoFixForWebRequestsWithSsl");
    ProviderInstaller.installIfNeeded(getApplicationContext());
    SSLContext sslContext;
    sslContext = SSLContext.getInstance("TLSv1.2");
    sslContext.init(null, null, null);
    sslContext.createSSLEngine();
    showMessage("DoFixForWebRequestsWithSsl completed!");
  }
}
