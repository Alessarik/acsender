package ru.alessarik.acsender;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * AsyncTask for prepare sending pictures
 */
public class SendAsyncTask extends AsyncTask<Void, Helper, Void> {

  private SendActivity mActivity;

  private String mtRegNomer;
  private String mtAddress;
  private String mtImagePath;

  private String mtEmail;
  private String mtSurname;
  private String mtName;
  private String mtTown;
  private String mtRegion;

  private Helper mtHelper;
  private boolean mtSended;

  SendAsyncTask(SendActivity aActivity, String aRegNomer, String aAddressStr, String aImagePath) {
    mActivity = aActivity;
    mtRegNomer = aRegNomer;
    mtAddress = aAddressStr;
    mtImagePath = aImagePath;
    mtSended = false;
    mtHelper = new Helper(this);
  }

  @Override
  protected Void doInBackground(Void... params) {
    try {
      checkRegNomer();
      checkAddress();
      mtEmail   = getConfigValue(Utils.APP_SAVED_EMAIL,   R.id.email);
      mtSurname = getConfigValue(Utils.APP_SAVED_SURNAME, R.id.surname);
      mtName    = getConfigValue(Utils.APP_SAVED_NAME,    R.id.name);
      mtTown    = getConfigValue(Utils.APP_SAVED_TOWN,    R.id.town);
      mtRegion  = getConfigValue(Utils.APP_SAVED_REGION,  R.id.spinner_region);
      mtRegion  = Utils.parseRegionCode(mtRegion);
      checkRegion();
      Utils.checkNetwork(mActivity, mtHelper);
      if (mtHelper.isNoError()) {
        mtSended = true;
      }
    } catch(Exception ex) {
      mtHelper.setError(ex.toString());
    }
    return null;
  }

  protected void checkRegNomer() {
    mtHelper.setStatus(mActivity.getString(R.string.status_check_regnumber));
    if(TextUtils.isEmpty(mtRegNomer)) {
      mtHelper.setError(mActivity.getString(R.string.error_empty_regnomer), false, R.id.regnomer, R.string.error_empty_regnomer);
      return;
    }
    String correctRegNomer = Utils.TransformRegNomer(mtRegNomer);
    mtHelper.setRegNomer(correctRegNomer);
    if(0 != mtRegNomer.compareTo(correctRegNomer)) {
      mtHelper.setError(mActivity.getString(R.string.error_correct_regnomer), false, R.id.regnomer, R.string.error_correct_regnomer);
      return;
    }
    if(!Utils.isRegNomerValid(correctRegNomer)) {
      mtHelper.setError(mActivity.getString(R.string.error_regnomer_format), false, R.id.regnomer, R.string.error_regnomer_format);
    }
  }

  protected void checkAddress() {
    mtHelper.setStatus(mActivity.getString(R.string.status_check_address));
    if(TextUtils.isEmpty(mtAddress)) {
      mtHelper.setError(mActivity.getString(R.string.error_field_required), false, R.id.address, R.string.error_field_required);
    }
  }

  protected void checkRegion() {
    mtHelper.setStatus(mActivity.getString(R.string.status_check_region));
    if(TextUtils.isEmpty(mtRegion)) {
      mtHelper.setError(mActivity.getString(R.string.error_field_required), true, R.id.spinner_region, R.string.error_field_required);
    }
  }

  protected String getConfigValue(String aKey, Integer viewId) {
    String value = mActivity.mSharedPreferences.getString(aKey, "");
    mtHelper.setStatus(aKey + ": " + value);
    if(TextUtils.isEmpty(value)) {
      mtHelper.setError(mActivity.getString(R.string.error_field_required), true, viewId, R.string.error_field_required);
    }
    return value;
  }

  public void publishProgress() {
    publishProgress(mtHelper);
    SystemClock.sleep(Utils.ASYNC_DELAY_TIME);
  }

  protected void onProgressUpdate(Helper... progress) {
    mActivity.showMessage(progress[0].getStatus());
    ProgressBar bar = (ProgressBar) mActivity.findViewById(R.id.progressbar);
    if(bar != null) {
      bar.setProgress(progress[0].getProgress());
    }
  }

  @Override
  protected void onPreExecute() {
    Button button = (Button) mActivity.findViewById(R.id.button_check);
    if (null != button) {
      button.setBackgroundColor(Color.YELLOW);
      button.setTextColor(Color.BLACK);
    }
    TextView nomerView = (TextView) mActivity.findViewById(R.id.regnomer);
    if (null != nomerView) {
      nomerView.setError(null);
    }
    TextView addressView = (TextView) mActivity.findViewById(R.id.address);
    if (null != addressView) {
      addressView.setError(null);
    }
  }

  @Override
  protected void onPostExecute(final Void success) {
    Button button = (Button) mActivity.findViewById(R.id.button_check);
    if (null != button) {
      button.setBackgroundColor(mtHelper.isNoError() ? Color.GREEN : Color.RED);
      button.setTextColor(Color.WHITE);

      Button button_pref = (Button) mActivity.findViewById(R.id.button_pref);
      if (null != button_pref) {
        LayoutParams params = button_pref.getLayoutParams();
        params.width = params.height = button.getHeight();
        button_pref.setLayoutParams(params);
        button_pref.setVisibility(View.VISIBLE);
      }
    }

    TextView nomerView = (TextView) mActivity.findViewById(R.id.regnomer);
    if (null != nomerView) {
      nomerView.setText(mtHelper.getRegNomer());
    }

    if (!mtHelper.isNoError()) {
      mActivity.showMessage(mtHelper.getError());
      if (mtHelper.isOpenView()) {
        Intent intent = new Intent(mActivity, PrefActivity.class);
        intent.putExtra(Utils.ERROR_VIEW_ID, mtHelper.getErrorViewId());
        intent.putExtra(Utils.ERROR_VIEW_CODE, mtHelper.getErrorViewCode());
        mActivity.startActivityForResult(intent, Utils.PREF_ACTIVITY);
      } else {
        if (0 != mtHelper.getErrorViewId()) {
          TextView view = (TextView) mActivity.findViewById(mtHelper.getErrorViewId());
          if (null != view) {
            if (0 != mtHelper.getErrorViewCode()) {
              view.setError(mActivity.getString(mtHelper.getErrorViewCode()));
            } else {
              view.setError(mtHelper.getError());
            }
            view.requestFocus();
          }
        }
      }
    }

    if(mtSended) {
      Button bCheckView = (Button) mActivity.findViewById(R.id.button_check);
      if(bCheckView != null) {
        bCheckView.setEnabled(false);
      }
      if(nomerView != null) {
        nomerView.setEnabled(false);
      }
      View addressView = mActivity.findViewById(R.id.address);
      if(addressView != null) {
        addressView.setEnabled(false);
      }
      mActivity.SendToGibdd(mtHelper.getRegNomer(), mtAddress, mtEmail, mtSurname, mtName, mtTown, mtRegion);
    }

    mActivity.mSendTask = null;
  }

  @Override
  protected void onCancelled() {
    Button button = (Button) mActivity.findViewById(R.id.button_check);
    if(button != null) {
      button.setBackgroundColor(Color.RED);
      button.setTextColor(Color.WHITE);
    }
    mActivity.mSendTask = null;
    mActivity.showMessage(mActivity.getString(R.string.error_send_cancelled));
  }
}
