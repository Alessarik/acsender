package ru.alessarik.acsender;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.Math;
import java.net.HttpURLConnection;
import java.util.UUID;

/**
 * Utility functions and common constants
 */
public class Utils {

  private static String TAG = "UTILS";

  private static String validAlphas   = "ABCEHKMOPTXY";
  private static String validRussians = null;
  private static String validDigits   = "0123456789";

  public static Integer GIBDD_ACTIVITY = 5;
  public static Integer PREF_ACTIVITY = 7;

  public static String APP_ID = "ID";
  public static String APP_SAVED_EMAIL = "SAVED_EMAIL";
  public static String APP_SAVED_SURNAME = "SAVED_SURNAME";
  public static String APP_SAVED_NAME = "SAVED_NAME";
  public static String APP_SAVED_TOWN = "SAVED_TOWN";
  public static String APP_SAVED_REGION = "SAVED_REGION";

  public static String ERROR_VIEW_ID = "ERROR_VIEW_ID";
  public static String ERROR_VIEW_CODE = "ERROR_VIEW_CODE";
  public static String IMAGE_ABS_PATH = "IMAGE_ABS_PATH";
  public static String REG_NOMER = "REG_NOMER";
  public static String ADDRESS = "ADDRESS";
  public static String EMAIL = "EMAIL";
  public static String SURNAME = "SURNAME";
  public static String NAME = "NAME";
  public static String TOWN = "TOWN";
  public static String REGION = "REGION";

  private static String CACHE = "cache";
  private static String STATISTICS = "statistics";
  private static String SENT_COUNT = "SENT_COUNT";
  private static String ADDRESS_KEY_FORMAT = "address_%d";
  private static String ADDRESS_VALUE_FORMAT = "address_count_%d";

  public  static Integer ASYNC_INSTRUCTION_DELAY = 200;
  public  static Integer ASYNC_DELAY_TIME = 1000;
  private static Integer SIZE_OF_BUFFER_CHUNK = 100;

  public static String LINE_END = "\r\n";
  public static String TWO_HYPHENS = "--";
  public static String BOUNDRY = "314159265353562951413";

  public static void logMessage(String str) {
    if (!TextUtils.isEmpty(str)) {
      Log.d(TAG, str);
    } else {
      Log.w(TAG, "<log empty string>");
    }
  }

  public static void initialize(Context context) {
    if (TextUtils.isEmpty(validRussians)) {
      validRussians = context.getString(R.string.valid_russians);
    }
  }

  public static Boolean isRegNomerValid(String regNomer) {
    if(regNomer == null)
      return false;
    if(regNomer.length() != 8 && regNomer.length() != 9)
      return false;
    if(validAlphas.indexOf(regNomer.charAt(0)) < 0)
      return false;
    if(validDigits.indexOf(regNomer.charAt(1)) < 0)
      return false;
    if(validDigits.indexOf(regNomer.charAt(2)) < 0)
      return false;
    if(validDigits.indexOf(regNomer.charAt(3)) < 0)
      return false;
    if(validAlphas.indexOf(regNomer.charAt(4)) < 0)
      return false;
    if(validAlphas.indexOf(regNomer.charAt(5)) < 0)
      return false;
    if(validDigits.indexOf(regNomer.charAt(6)) < 0)
      return false;
    if(validDigits.indexOf(regNomer.charAt(7)) < 0)
      return false;
    if(regNomer.length() == 9 && validDigits.indexOf(regNomer.charAt(8)) < 0)
      return false;
    return true;
  }

  public static String TransformRegNomer(String regNomer) {
    String upper = regNomer.toUpperCase();
    StringBuffer buffer = new StringBuffer();
    for (int i = 0; i < regNomer.length(); i++) {
      char ch = upper.charAt(i);
      if (validAlphas.indexOf(ch) >= 0) {
        buffer.append(ch);
        continue;
      }
      if (validDigits.indexOf(ch) >= 0) {
        buffer.append(ch);
        continue;
      }
      int index = validRussians.indexOf(ch);
      if (index >= 0) {
        buffer.append(validAlphas.charAt(index));
      }
    }
    return buffer.toString();
  }

  public static String parseCode(String input) {
    Integer index = input.indexOf("\"code\"");
    if(index > -1) {
      index = input.indexOf(':', index);
      if(index > -1) {
        index = input.indexOf('"', index);
        if(index > -1) {
          Integer end = input.indexOf('"', index + 1);
          if(end > -1) {
            return input.substring(index + 1, end);
          }
        }
      }
    }
    return null;
  }

  public static String parseEMail(String input) {
    int index = input.indexOf("EMAIL");
    if(index > -1) {
      index = input.lastIndexOf("<", index);
      index = (index > -1) ? index : 0;
      index = input.indexOf("value", index);
      if(index > -1) {
        index = input.indexOf("\"", index);
        if(index > -1) {
          int start = index + 1;
          int finish = input.indexOf("\"", start);
          if(finish > -1) {
            return input.substring(start, finish);
          }
        }
      }
    }
    return null;
  }

  public static String parseFToken(String input) {
    int index = input.indexOf("captcha.jpeg");
    if(index > -1) {
      index = input.indexOf("?", index);
      if(index > -1) {
        int start = index + 1;
        int finish = input.indexOf("\"", index);
        if(finish > -1) {
          return input.substring(start, finish);
        }
      }
    }
    return null;
  }

  public static String parseSession(String input) {
    int index = input.indexOf("session=");
    if (index > -1) {
      index = input.indexOf("=", index);
      if (index > -1) {
        int start = index + 1;
        int finish = input.indexOf(";", index);
        if (finish > -1) {
          return input.substring(start, finish);
        }
      }
    }
    return null;
  }

  public static Integer parseFileId(String input) {
    int index = input.indexOf("file");
    if (index > -1) {
      index = input.indexOf("id", index);
      if (index > -1) {
        index = input.indexOf(":", index);
        if (index > -1) {
          index = input.indexOf("\"", index);
          if (index > -1) {
            int start = index + 1;
            int finish = input.indexOf("\"", start);
            if (finish > -1) {
              String id = input.substring(start, finish);
              return Integer.parseInt(id);
            }
          }
        }
      }
    }
    return null;
  }

  public static Boolean checkSuccess(String input) {
    int index = input.indexOf("success");
    if (index > -1) {
      index = input.indexOf(":", index);
      if (index > -1) {
        int start = index + 1;
        String substring = input.substring(start, start + 4);
        if (0 == substring.compareTo("true")) {
          return true;
        }
      }
    }
    return false;
  }

  // INFO: Strings with russian letters can be combined with wrong style.
  // It happens if border of SIZE_OF_BUFFER_CHUNK divides russian letter on to parts (in UTF8).
  public static String getInputStreamAsString(HttpURLConnection connection) throws Exception {
    InputStream stream = new BufferedInputStream(connection.getInputStream());
    byte[] buffer = new byte[SIZE_OF_BUFFER_CHUNK];
    StringBuilder stringBuilder = new StringBuilder();
    while(true) {
      Integer rd = stream.read(buffer, 0, buffer.length);
      if(rd < 0) {
        break;
      }
      String chunk = new String(buffer);
      int length = Math.min(rd, chunk.length());
      stringBuilder.append(chunk.substring(0, length));
    }
    stream.close();
    return stringBuilder.toString();
  }

  public static String parseRegionCode(String input) {
    if(!TextUtils.isEmpty(input)) {
      String output = input.substring(0, 2);
      try {
        Integer.parseInt(output);
        return output;
      }
      catch(Exception ex) {
      }
    }
    return null;
  }

  public static Pair<Boolean, String> DecodeClaimResult(String input) {
    Integer start = input.indexOf("b-message");
    if (start > -1) {
      start = input.indexOf(">", start);
      if (start > -1) {
        start++;
        Integer finish = input.indexOf("clean", start);
        if (finish > -1) {
          finish = input.lastIndexOf("<", finish);
          if (finish > -1) {
            String answer = input.substring(start, finish);
            return Pair.create(true, answer);
          }
        }
      }
    }

    start = input.indexOf("var errors");
    if (start > -1) {
      start = input.indexOf("{", start);
      if (start > -1) {
        start++;
        Integer finish = input.indexOf("}", start);
        if (finish > -1) {
          String answer = input.substring(start, finish);
          return Pair.create(false, answer);
        }
      }
    }

    return null;
  }

  public static String DecodeHtmlContent(String input) {
    Boolean flagContent = true;
    StringBuffer buffer = new StringBuffer();
    for (int i = 0; i < input.length(); i++) {
      char ch = input.charAt(i);
      if ('<' == ch) {
        flagContent = false;
        continue;
      }
      if ('>' == ch) {
        flagContent = true;
        continue;
      }
      if (flagContent) {
        buffer.append(ch);
      }
    }
    return buffer.toString();
  }

  public static String extractNameFromPath(String path) {
    int index = path.lastIndexOf("/");
    if (-1 < index && index < path.length()) {
      return path.substring(index + 1);
    }
    return path;
  }

  public static String extractDigitsFromPath(String path) {
    StringBuilder digits = new StringBuilder();
    for (int index = path.lastIndexOf("/"); index < path.length(); index++) {
      if (Character.isDigit(path.charAt(index))) {
        digits.append(path.charAt(index));
      }
    }
    return digits.toString();
  }

  public static String extractDateFromPath(String path) {
    String digits = extractDigitsFromPath(path);
    String date = digits.substring(6, 8) + "." + digits.substring(4, 6) + "." + digits.substring(0, 4);
    return date;
  }

  public static String extractTimeFromPath(String path) {
    String digits = extractDigitsFromPath(path);
    String time = digits.substring(8, 10) + ":" + digits.substring(10, 12);
    return time;
  }

  public static void appendFormData(StringBuffer buffer, String name, String data) {
    buffer.append(TWO_HYPHENS);
    buffer.append(BOUNDRY);
    buffer.append(LINE_END);
    buffer.append("Content-Disposition: form-data; name='");
    buffer.append(name);
    buffer.append("'");
    buffer.append(LINE_END);
    buffer.append(LINE_END);
    buffer.append(data);
    buffer.append(LINE_END);
  }

  public static byte[] appendFormFile(StringBuffer buffer, String formDataName, String additionalInfo, String filePath) throws Exception {
    buffer.append(TWO_HYPHENS);
    buffer.append(BOUNDRY);
    buffer.append(LINE_END);
    buffer.append("Content-Disposition: form-data; name='");
    buffer.append(formDataName);
    buffer.append("'; filename='");
    buffer.append(extractNameFromPath(filePath));
    buffer.append("'");
    buffer.append(LINE_END);
    buffer.append(additionalInfo);
    buffer.append(LINE_END);
    buffer.append(LINE_END);
    return getFileContent(filePath);
  }

  public static void appendFormEnd(StringBuffer buffer) {
    buffer.append(LINE_END);
    buffer.append(TWO_HYPHENS);
    buffer.append(BOUNDRY);
    buffer.append(TWO_HYPHENS);
    buffer.append(LINE_END);
  }

  public static boolean checkNetwork(Activity activity, Helper mtHelper) {
    mtHelper.setStatus(activity.getString(R.string.status_check_network));
    mtHelper.setNetwork(false);
    ConnectivityManager connManager = (ConnectivityManager) activity.getSystemService(
                                        activity.getBaseContext().CONNECTIVITY_SERVICE);
    NetworkInfo activeInfo = connManager.getActiveNetworkInfo();
    if (activeInfo != null && activeInfo.isConnected()) {
      int type = activeInfo.getType();
      if (ConnectivityManager.TYPE_WIFI == type) {
        mtHelper.setNetwork(true);
      } else if (ConnectivityManager.TYPE_MOBILE == type) {
        mtHelper.setNetwork(true);
      }
    }
    if (!mtHelper.getNetwork()) {
      mtHelper.setError(activity.getString(R.string.error_internet_unavailable));
    }
    return mtHelper.getNetwork();
  }

  public static byte[] getFileContent(String filePath) throws Exception {
    File file = new File(filePath);
    FileInputStream fileInputStream = new FileInputStream(file);
    int bufferSize = fileInputStream.available();
    byte[] buffer = new byte[bufferSize];
    int bytesRead = fileInputStream.read(buffer, 0, bufferSize);
    fileInputStream.close();
    return buffer;
  }

  public static String getStringFromRawFile(Activity activity, Integer resourceId) {
    InputStream stream = activity.getResources().openRawResource(resourceId);
    String output = null;
    try {
      output = convertStreamToString(stream);
    } catch (Exception e) {
    }
    try {
      stream.close();
    } catch (Exception e) {
    }
    return output;
  }

  private static String convertStreamToString(InputStream input) throws Exception {
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    int i = input.read();
    while(i != -1)
    {
      stream.write(i);
      i = input.read();
    }
    return stream.toString();
  }

  public static String getAbsolutePath(Context context, Uri uri) {
    if ("content".equalsIgnoreCase(uri.getScheme())) {
      Cursor cursor = null;
      try {
        cursor = context.getContentResolver().query(uri, null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
          int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
          return cursor.getString(column_index);
        }
      } finally {
        if (cursor != null) {
          cursor.close();
        }
      }
    }
    return null;
  }

  public static String replacePattern(String input, String pattern, String string) {
    if (!TextUtils.isEmpty(input) && !TextUtils.isEmpty(pattern) && !TextUtils.isEmpty(string)) {
      return input.replace(pattern, string);
    }
    return input;
  }

  public static void addSentCountStats(Context context) {
    SharedPreferences settings = context.getSharedPreferences(STATISTICS, Context.MODE_PRIVATE);
    int count = 1 + settings.getInt(SENT_COUNT, 0);
    Editor editor = settings.edit();
    editor.putInt(SENT_COUNT, count);
    editor.apply();
  }

  public static int getSentCountStats(Context context) {
    SharedPreferences settings = context.getSharedPreferences(STATISTICS, Context.MODE_PRIVATE);
    return settings.getInt(SENT_COUNT, 0);
  }

  public static void addAddressToCache(Context context, String address) {
    if (TextUtils.isEmpty(address)) {
      return;
    }
    SharedPreferences settings = context.getSharedPreferences(CACHE, Context.MODE_PRIVATE);
    int iterator = 0;
    while (true) {
      String keyName = String.format(ADDRESS_KEY_FORMAT, iterator);
      String key = settings.getString(keyName, "");
      if (0 == key.compareToIgnoreCase(address)) {
        String valueName = String.format(ADDRESS_VALUE_FORMAT, iterator);
        Editor editor = settings.edit();
        int value = 1 + settings.getInt(valueName, 0);
        editor.putInt(valueName, value);
        editor.apply();
        break;
      }
      if (TextUtils.isEmpty(key)) {
        String valueName = String.format(ADDRESS_VALUE_FORMAT, iterator);
        Editor editor = settings.edit();
        editor.putString(keyName, address);
        editor.putInt(valueName, 1);
        editor.apply();
        break;
      }
      iterator++;
    }
  }

  public static String getMaxUsedAddress(Context context) {
    SharedPreferences settings = context.getSharedPreferences(CACHE, Context.MODE_PRIVATE);
    int maxCounter = 0;
    String maxValue = null;
    int iterator = 0;
    while (true) {
      String valueName = String.format(ADDRESS_VALUE_FORMAT, iterator);
      int counter = settings.getInt(valueName, -1);
      if (counter >= maxCounter) {
        String keyName = String.format(ADDRESS_KEY_FORMAT, iterator);
        String address = settings.getString(keyName, "");
        if (!TextUtils.isEmpty(address)) {
          maxValue = address;
        }
      }
      if (counter < 0) {
        break;
      }
      iterator++;
    }
    return maxValue;
  }

  public static String getAppId(Context context) {
    SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
    String id = settings.getString(APP_ID, "");
    if (!TextUtils.isEmpty(id)) {
      return id;
    }
    id = UUID.randomUUID().toString();
    Editor editor = settings.edit();
    editor.putString(APP_ID, id);
    editor.apply();
    return id;
  }

  public static void sendTelemetry(Context context) throws Exception {
    String path = context.getString(R.string.url_telemetry);
    String id = getAppId(context);
    id = id.substring(0, 2).toUpperCase();
    Integer count = getSentCountStats(context);

    RequestHelper request = RequestHelper.PostUrlEncodedRequest();
    request.setAddress(path);
    request.addParam("appId", id);
    request.addParam("sentCount", count.toString());
    request.doRequest();
  }

  public static String decodeRussian(String input, Activity activity) throws Exception {
    String output = input;
    output = output.replace("\\u0410", activity.getString(R.string.A));
    output = output.replace("\\u0411", activity.getString(R.string.B));
    output = output.replace("\\u0412", activity.getString(R.string.V));
    output = output.replace("\\u0413", activity.getString(R.string.G));
    output = output.replace("\\u0414", activity.getString(R.string.D));
    output = output.replace("\\u0415", activity.getString(R.string.E));
    output = output.replace("\\u0416", activity.getString(R.string.ZH));
    output = output.replace("\\u0417", activity.getString(R.string.Z));
    output = output.replace("\\u0418", activity.getString(R.string.I));
    output = output.replace("\\u0419", activity.getString(R.string.J));
    output = output.replace("\\u041a", activity.getString(R.string.K));
    output = output.replace("\\u041b", activity.getString(R.string.L));
    output = output.replace("\\u041c", activity.getString(R.string.M));
    output = output.replace("\\u041d", activity.getString(R.string.N));
    output = output.replace("\\u041e", activity.getString(R.string.O));
    output = output.replace("\\u041f", activity.getString(R.string.P));
    output = output.replace("\\u0420", activity.getString(R.string.R));
    output = output.replace("\\u0421", activity.getString(R.string.S));
    output = output.replace("\\u0422", activity.getString(R.string.T));
    output = output.replace("\\u0423", activity.getString(R.string.Y));
    output = output.replace("\\u0424", activity.getString(R.string.F));
    output = output.replace("\\u0425", activity.getString(R.string.KH));
    output = output.replace("\\u0426", activity.getString(R.string.C));
    output = output.replace("\\u0427", activity.getString(R.string.CH));
    output = output.replace("\\u0428", activity.getString(R.string.SH));
    output = output.replace("\\u0429", activity.getString(R.string.SCH));
    output = output.replace("\\u042a", activity.getString(R.string.HH));
    output = output.replace("\\u042b", activity.getString(R.string.IH));
    output = output.replace("\\u042c", activity.getString(R.string.JH));
    output = output.replace("\\u042d", activity.getString(R.string.EH));
    output = output.replace("\\u042e", activity.getString(R.string.JU));
    output = output.replace("\\u042f", activity.getString(R.string.JA));

    output = output.replace("\\u0430", activity.getString(R.string.a));
    output = output.replace("\\u0431", activity.getString(R.string.b));
    output = output.replace("\\u0432", activity.getString(R.string.v));
    output = output.replace("\\u0433", activity.getString(R.string.g));
    output = output.replace("\\u0434", activity.getString(R.string.d));
    output = output.replace("\\u0435", activity.getString(R.string.e));
    output = output.replace("\\u0436", activity.getString(R.string.zh));
    output = output.replace("\\u0437", activity.getString(R.string.z));
    output = output.replace("\\u0438", activity.getString(R.string.i));
    output = output.replace("\\u0439", activity.getString(R.string.j));
    output = output.replace("\\u043a", activity.getString(R.string.k));
    output = output.replace("\\u043b", activity.getString(R.string.l));
    output = output.replace("\\u043c", activity.getString(R.string.m));
    output = output.replace("\\u043d", activity.getString(R.string.n));
    output = output.replace("\\u043e", activity.getString(R.string.o));
    output = output.replace("\\u043f", activity.getString(R.string.p));
    output = output.replace("\\u0440", activity.getString(R.string.r));
    output = output.replace("\\u0441", activity.getString(R.string.s));
    output = output.replace("\\u0442", activity.getString(R.string.t));
    output = output.replace("\\u0443", activity.getString(R.string.y));
    output = output.replace("\\u0444", activity.getString(R.string.f));
    output = output.replace("\\u0445", activity.getString(R.string.kh));
    output = output.replace("\\u0446", activity.getString(R.string.c));
    output = output.replace("\\u0447", activity.getString(R.string.ch));
    output = output.replace("\\u0448", activity.getString(R.string.sh));
    output = output.replace("\\u0449", activity.getString(R.string.sch));
    output = output.replace("\\u044a", activity.getString(R.string.hh));
    output = output.replace("\\u044b", activity.getString(R.string.ih));
    output = output.replace("\\u044c", activity.getString(R.string.jh));
    output = output.replace("\\u044d", activity.getString(R.string.eh));
    output = output.replace("\\u044e", activity.getString(R.string.ju));
    output = output.replace("\\u044f", activity.getString(R.string.ja));
    return output;
  }
}
